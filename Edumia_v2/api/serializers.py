from rest_framework import serializers
from students import models
from phonenumber_field.serializerfields import PhoneNumberField


class StudentsProfileSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(
        validators=PhoneNumberField().validators,)
    mother_phone_number = serializers.CharField(
        validators=PhoneNumberField().validators, )
    class Meta:
        model = models.StudentsProfile
        fields = '__all__'
class StudentCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StudentCategory