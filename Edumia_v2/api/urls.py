from rest_framework import routers
from .views import *
from django.urls import path,include
from api import views



#api
router = routers.DefaultRouter()
router.register(r'student_list',StudentViewSet)
router.register(r'category_list',StudentCategoryViewSet)
urlpatterns = [path('usercheck',views.usercheck,name='usercheck')]+router.urls