import requests
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from .serializers import StudentsProfileSerializer,StudentCategorySerializer
from rest_framework import viewsets
from students.models import StudentsProfile,StudentCategory
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import DjangoModelPermissions,IsAuthenticated,IsAdminUser
from api.permissions import IsTeacher
from django.shortcuts import HttpResponse


class StudentViewSet(viewsets.ModelViewSet):
    queryset = StudentsProfile.objects.all()
    serializer_class = StudentsProfileSerializer
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsTeacher|IsAdminUser]



class StudentCategoryViewSet(viewsets.ModelViewSet):
    queryset = StudentCategory.objects.all()
    serializer_class = StudentCategorySerializer


def usercheck(request):

    groups = request.auth
    return HttpResponse(groups)