from rest_framework import permissions


class IsTeacher(permissions.BasePermission):
    def has_permission(self,request,view):
        if request.user.groups.filter(name='Teacher'):
            return True
        return False
