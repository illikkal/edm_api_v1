from django.contrib import admin
from klass.models import SubjectTeacher


# Register your models here.
class SubjectTeacherAdmin(admin.ModelAdmin):
    fields = ['division', 'subject', 'teacher']


admin.site.register(SubjectTeacher, SubjectTeacherAdmin)
