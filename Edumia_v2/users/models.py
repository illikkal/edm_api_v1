from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from students.models import StudentsProfile
from Staff.models import StaffProfile
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from .managers import CustomUserManager


class SessionCalendar(models.Model):
    # todo: change months like sep oct etc with choice
    months = (('January', 'January'),
              ('February', 'February'),
              ('March', 'March'),
              ('April', 'April'),
              ('May', 'May'),
              ('June', 'June'),
              ('July', 'July'),
              ('August', 'August'),
              ('September', 'September'),
              ('October', 'October'),
              ('November', 'November'),
              ('December', 'December'))
    start_month = models.CharField(max_length=32, blank=True, null=True, choices=months)
    start_year = models.IntegerField(blank=True, null=True)
    end_month = models.CharField(max_length=32, blank=True, null=True, choices=months)
    end_year = models.IntegerField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return '{} {} - {} {}'.format(
            self.start_month or '', self.start_year or '',
            self.end_month or '', self.end_year
        )

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True
        self.save()


class CustomUser(AbstractBaseUser, PermissionsMixin):
    number = models.CharField(max_length=100, unique=True)
    email = models.EmailField()
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    session_start = models.ForeignKey(
        SessionCalendar, blank=True, null=True, on_delete=models.CASCADE,
        related_name='session_start'
    )
    session_end = models.ForeignKey(
        SessionCalendar, blank=True, null=True, on_delete=models.CASCADE,
        related_name='session_end'
    )
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)
    USERNAME_FIELD = 'number'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.number


class UserHistory(models.Model):
    """
    object to be created on new student joining in a session, being promoted or failed
    being transferred in the same session, for teachers added
    teachers continuing in school should have new entry. to keep the history of
    subjects and classes taught in previous sessions
    """

    user = models.ForeignKey(CustomUser, blank=True, null=True,
                             on_delete=models.CASCADE)

    user_choice = (('student', 'student'),
                   ('staff', 'staff'))
    user_type = models.CharField(max_length=32, choices=user_choice)
    enroll_data = models.TextField(max_length=2056, blank=True)
    session = models.ForeignKey(SessionCalendar, blank=True, null=True, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)

    def fetch_enroll_data(self):
        mapper = {
            'student': Students_Profile,
            'staff': StaffProfile
        }
        data = mapper['student'].session_data()
        self.enroll_data = data
        self.save()
