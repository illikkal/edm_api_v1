from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, UserHistory

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import SessionCalendar


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ('number', 'is_staff', 'is_active',)
    list_filter = ('number', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('number', 'password')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('number', 'email','password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('number',)
    ordering = ('number',)


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(SessionCalendar)
admin.site.register(UserHistory)