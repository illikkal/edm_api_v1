from django.shortcuts import render
from students.models import StudentsProfile,Division,StudentCategory
from django.http import HttpResponse
from django.shortcuts import redirect
from .forms import studentprofileform
from django.views.generic import CreateView
from django.urls import reverse_lazy



# Create your views here.
def pending(request):
    pending_students=StudentsProfile.objects.all()
    return render(request, 'students/students_profile.html', {'sp': pending_students})

def status_approvel(request):
    if request.method == 'GET':
        student_id = request.GET.get('id')
        student = StudentsProfile.objects.get(id=student_id)
        student.status = '1'
        student.save()
        pending_students = StudentsProfile.objects.all()
        return render(request, 'students/students_profile.html', {'sp': pending_students})
def status_denial(request):
    student_id = request.GET.get('id')
    student = StudentsProfile.objects.get(id=student_id)
    student.status = '2'
    student.save()
    pending_students = StudentsProfile.objects.all()
    return render(request, 'students/students_profile.html', {'sp': pending_students})
def base(request):
    return render(request,'students/base.html')

class AddStudent(CreateView):
    model = StudentsProfile
    form_class = studentprofileform
    success_url = reverse_lazy('success')
def success(request):
    return HttpResponse('student added successfully')

def add(request):
    form = studentprofileform
    return render(request,'students/studentsprofile_form.html',{'form':form})
def get_divisions(request):
    klass_id = request.GET.get('klass')
    divisions = Division.objects.filter(klass_id=klass_id).order_by('name')
    return render(request, 'students/drop_down_divisions.html', {'divisions': divisions})



def all_students(request):
    response = requests.get('http://127.0.0.1:8000/students/student_list/')
    students = response.json()
    return render(request,'students/allstudents.html',{'data':students})





