from django.urls import path,include
from students import views


urlpatterns = [path('pending',views.pending,name="pending"),
               path('status_approval',views.status_approvel,name="approvel"),
               path('status_denial',views.status_denial,name="denial"),
               path('base',views.base,name="base"),
               path('add_student/', views.AddStudent.as_view(), name='add_student'),
               path('reverse',views.success,name="success"),
               path('ajax/get_divisions/', views.get_divisions, name='ajax_get_divisions'),
               path('all_students',views.all_students,name = 'all_students'),
               path('add',views.add,name='add'),
               ]