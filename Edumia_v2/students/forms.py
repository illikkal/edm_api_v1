from django import forms
from .models import StudentsProfile, StudentCategory
from klass.models import Klass,Division


class studentprofileform(forms.ModelForm):

    class Meta:
        model = StudentsProfile
        fields = ['admission_number', 'admission_date', 'klass', 'division',
                  'first_name', 'last_name',
                  'birthday', 'gender',
                  'birth_place', 'nationality',
                  'mothertounge', 'category',
                  'religion', 'caste', 'caste_category',
                  'address_line1', 'address_line2',
                  'city', 'state',
                  'pin_code', 'country',
                  'father_name', 'phone_number',
                  'mother_name', 'mother_phone_number',
                  'aadhaar_Number', 'email',
                  'height', 'weight', 'blood_group',
                  'vaccination_details','user']

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['division'].queryset = Division.objects.none()
