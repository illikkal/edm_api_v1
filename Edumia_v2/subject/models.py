from django.db import models

# Create your models here.
class Subject(models.Model):
    name = models.CharField(max_length=100)
    klass = models.ForeignKey('klass.Klass', on_delete=models.CASCADE)


    def __str__(self):
        return '{0} ,class {1}'.format(self.name,self.klass)

