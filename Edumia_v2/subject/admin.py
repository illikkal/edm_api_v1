from django.contrib import admin
from subject.models import Subject

# Register your models here.
class SubjectAdmin(admin.ModelAdmin):
    fields = ['name','klass']


admin.site.register(Subject,SubjectAdmin)