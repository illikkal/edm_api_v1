from django.contrib.auth.backends import ModelBackend
from Staff.models import StaffProfile
from django.contrib.auth import get_user_model

class EmailBackend(ModelBackend):
    def authenticate(self,request, **kwargs):
        email = kwargs['username']

        password = kwargs['password']
        MyUser = get_user_model()
        try:
            employee = StaffProfile.objects.get(email=email)
            if employee.user.check_password(password) is True:
                return employee.user
        except employee.DoesNotExist:
            pass