from django.db import models
from django.conf import settings
from users.managers import CustomUserManager


class Departments(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class StaffCategory(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
class Designation(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
class Grade(models.Model):
    grade = models.CharField(max_length=50)

    def __str__(self):
        return self.grade
class StaffProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    employee_id = models.CharField(max_length=20,unique=True)
    first_name = models.CharField(max_length=100,verbose_name='First Name')
    last_name = models.CharField(max_length=100,verbose_name='Last Name')
    email = models.EmailField(max_length=100,unique=True)
    contact_number = models.CharField(max_length=13,unique=True)
    gender_choices=(
        ('M','Male'),
        ('F','Female'),
        ('O','Other'),
    )
    Blood_choices=(
        ('A+','A POSITIVE'),
        ('A-','A NEGATIVE'),
        ('B+', 'B POSITIVE'),
        ('B-','B NEGATIVE'),
        ('O+','O POSITIVE'),
        ('O-','O NEGATIVE'),
        ('AB+','AB POSITIVE'),
        ('AB-','AB NEGATIVE')
    )
    marital_choices=(
        ('S','Single'),
        ('M','Married'),
        ('D','Divorced'),
        ('W','Widowed')
    )
    gender = models.CharField(max_length=1,choices=gender_choices,default='O',help_text="Select gender")
    date_of_birth = models.DateField(null=True, blank=True)
    blood_group = models.CharField(max_length=3,choices=Blood_choices,)
    department = models.ForeignKey('Departments',on_delete=models.SET_NULL,null=True)
    is_teacher = models.BooleanField()
    category = models.ForeignKey('StaffCategory',on_delete=models.SET_NULL,null=True)
    designation = models.ForeignKey('Designation',on_delete=models.SET_NULL,null=True)
    qualification = models.CharField(max_length=100,null=True)
    experience = models.CharField(max_length=100,null=True)
    date_joined = models.DateField()
    grade = models.ForeignKey('Grade',on_delete=models.SET_NULL,null=True)
    marital_status = models.CharField(max_length=1,choices=marital_choices)
    no_of_children = models.IntegerField()
    father_name = models.CharField(max_length=50)
    mother_name = models.CharField(max_length=50)

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    def session_data(self):
        return str(self.first_name)





