from django.shortcuts import render,redirect,HttpResponse
from users.forms import CustomUserCreationForm,CustomUserChangeForm
from .models import StaffProfile
from .forms import *

def add_staff(request):
    if request.method == 'POST':
        staff_form = StaffCreationForm(prefix="staff",data=request.POST)
        profile_form = ProfileForm(prefix='profile',data=request.POST)
        if staff_form.is_valid() and profile_form.is_valid():
            staff=staff_form.save()
            profile = profile_form.save(commit=False)
            profile.user=staff
            profile.save()
            return HttpResponse('added')
    else:
        staff_form = StaffCreationForm(prefix='staff')
        profile_form = ProfileForm(prefix='profile')
    return render(request,'Staff/create_staff.html',{'staff_form':staff_form,'profile_form':profile_form})


