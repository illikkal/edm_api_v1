from django.contrib.auth.backends import ModelBackend
from Staff.models import StaffProfile
from django.contrib.auth import get_user_model
from django.db.models import Q

class StaffBackend(ModelBackend):
    def authenticate(self,request, **kwargs):
        employee_id = kwargs['username']
        email = kwargs['username']

        password = kwargs['password']
        MyUser = get_user_model()
        try:
            employee = StaffProfile.objects.get(Q(employee_id=employee_id) | Q(email=email))
            if employee.user.check_password(password) is True:
                return employee.user
        except StaffProfile.DoesNotExist:
            pass
