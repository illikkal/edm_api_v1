from django import forms
from .models import StaffProfile,StaffCategory,Departments,Designation,Grade
from users.forms import CustomUserCreationForm
from users.models import CustomUser



class StaffCreationForm(CustomUserCreationForm):
    number = forms.CharField(max_length=30,help_text='Required')

    class Meta:
        model = CustomUser
        fields = ('number','password1','password2')

class ProfileForm(forms.ModelForm):

    class Meta:
        model = StaffProfile
        fields = (
            'employee_id',
            'first_name',
            'last_name',
            'email',
            'contact_number',
            'gender',
            'date_of_birth',
            'blood_group',
            'department',
            'is_teacher',
            'category',
            'designation',
            'qualification',
            'experience',
            'date_joined',
            'grade',
            'marital_status',
            'no_of_children',
            'father_name',
            'mother_name','user',

        )
        exclude = ('user',)


from django import forms
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import Group


User = get_user_model()


# Create ModelForm based on the Group model.
class GroupAdminForm(forms.ModelForm):
    class Meta:
        model = Group
        exclude = []

    # Add the users field.
    users = forms.ModelMultipleChoiceField(
         queryset=User.objects.all(),
         required=False,
         # Use the pretty 'filter_horizontal widget'.
         widget=FilteredSelectMultiple('users', False)
    )

    def __init__(self, *args, **kwargs):
        # Do the normal form initialisation.
        super(GroupAdminForm, self).__init__(*args, **kwargs)
        # If it is an existing group (saved objects have a pk).
        if self.instance.pk:
            # Populate the users field with the current Group users.
            self.fields['users'].initial = self.instance.user_set.all()

    def save_m2m(self):
        # Add the users to the Group.
        self.instance.user_set.set(self.cleaned_data['users'])

    def save(self, *args, **kwargs):
        # Default save
        instance = super(GroupAdminForm, self).save()
        # Save many-to-many data
        self.save_m2m()
        return instance